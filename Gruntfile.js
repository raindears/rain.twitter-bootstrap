module.exports = function(grunt) {
  'use strict';

  grunt.config.init({

    dirs: {
      less: 'node_modules/bootstrap/less',
      js: 'node_modules/bootstrap/js',
      tmp: 'tmp'
    },

    files: {
      less: [ '<%= dirs.less %>/*.less', '!<%= dirs.less %>/{bootstrap,responsive}*.less' ],
      tmpLess: [ '<%= dirs.tmp %>/*.less', '!<%= dirs.tmp %>/{variables,mixins}.less' ],
      tmpCss: '<%= dirs.tmp %>/*.css',
      js : '<%= dirs.js %>/bootstrap-*.js'
    }

  });

  grunt.registerTask('build', function() {

    if (! grunt.file.exists(grunt.config.get('dirs.less'))) {
      grunt.fail.fatal('Twitter bootstrap is not in path. Please type "npm install" before building this component.');
    }

    var tmp;

    try {

      tmp = config('dirs.tmp');

      // create a tmp directory and copy the bootstrap LESS files to it
      grunt.file.mkdir(tmp);
      expandFiles('files.less').forEach(copyTo(tmp));

      // add missing imports to the LESS files and convert them to CSS
      var lessFiles = expandFiles('files.tmpLess');

      lessFiles.forEach(addImports(tmp, [ 'variables', 'mixins' ]));
      lessFiles.forEach(saveAsCss);

      // copy CSS and JS files to the client directories
      expandFiles('files.tmpCss').forEach(copyTo('resources'));
      expandFiles('files.js').forEach(copyTo('client/js'));

    } finally {

      // ensure deletion of the tmp directory
      grunt.file.delete(tmp);

    }

    function expandFiles(confKey) {
      return grunt.file.expand(config(confKey));
    }

    function config(key) {
      return grunt.config.get(key);
    }

    function copyTo(dir) {
      return function(file) {
        grunt.file.copy(file, dir + '/' + file.split('/').pop());
      };
    }

    function addImports(dir, imports) {
      imports = [].concat(imports).map(function(x) {
        return grunt.file.read(dir + '/' + x + '.less');
      }).join('\n');

      return function(file) {
        grunt.file.copy(file, file, { process: function(text) { return imports + '\n' + text; }});
      };
    }

    function saveAsCss(file) {
      require('less').render(grunt.file.read(file), { compress: true }, function(e, css) {
        grunt.file.write(file.replace(/\.less$/, '.css'), css);
      });
    }

  });

};

